;; I didn't really want to do this but it seems the simplest way
(in-package :asdf/user)

;; PACKAGE-FILE - a version of SOURCE-FILE / :file which allows specifying what packages are contained
(defclass package-file (cl-source-file)
	;; ASDF once used to have property lists for putting arbitrary things on components, but now apparently you can only subclass them. so I did.
	;; http://sbcl.org/asdf/Creating-new-component-types.html#Creating-new-component-types
	((packages :accessor file-packages :initarg :packages)))

(defmethod perform ((o compile-op) (c package-file))
	;; whenever a PACKAGE-FILE is compiled, register its system as containing its packages
	;; https://stackoverflow.com/questions/65573027/using-asdfs-around-compile-for-individual-files
	(register-system-packages (component-system c) (file-packages c))
	(asdf/lisp-action:perform-lisp-compilation o c))


;; to use this with an asdf system, put this form before DEFSYSTEM:
;;   (asdf:load-system "grevillea")
;; :defsystem-depends-on has always been broken sadly
;; https://www.mail-archive.com/asdf-devel%40common-lisp.net/msg05369.html
