;;;; grevillea / vxsh Calendar Versioning package
(defpackage :vskal
	(:use :common-lisp)
	;;(:local-nicknames
		;; :asdf
	;;)
	(:export
		
		;; release-integer-letter
		;; #::version-string-helper
		
		;; VERSION data type
		;; #::date-field
		#:version #:make-version #:copy-version
		#:version-year #:version-month #:version-day #:version-release
		
		;; converting other things to VERSION
		#:sequence-to-version #:time-to-version
		; string-to-version - unfinished
		#:parse-version
		;; converting VERSION to other things
		#:version-to-time
		
		#:next-release #:release-a
		#:version= #:version< #:version>
		
		#:get-calendar-version
		
		;; LATER: defer to other packages?
))
(in-package :vskal)



(defun release-integer-letter (release-integer) ; release number to single letter {{{
	(let ((BACKTICK 96))
	
	;; if within a-z, use letter, otherwise, use decimal
	(if (<= release-integer 26)
		;; to get a letter, add the first relevant ASCII character to each place
		(code-char
			(+ BACKTICK release-integer))
		release-integer)
	
	;; LATER: finish code to create base-26 string
	;; I really want aa - az to be possible for some reason
	;; although it's unclear if in practice aa-az or 27-52 would be more standard
)) ; }}}

#| failed attempt to create a real base-26 string; revisit later?

(defun release-integer-letter (release-integer) ; release integer to letter
	(let ((release
			release-integer
			;;(if (< release-integer 2)
				;;0  (- release-integer 1))
				)
		(SMALL-A 97) (BASE 26) i base-26)
	
	(loop  while (or (> release 0) (null base-26))  do
		(multiple-value-bind (main remainder) ; capture result & remainder...
			(truncate release BASE) ; ...from RELEASE-INTEGER / base
			;; and with these:
			(setq release remainder)  ; RELEASE decreases
			(push  ; MAIN moves onto BASE-26 number
				(code-char  ; to get a letter,
					;; add the ASCII small-a character to each place
					(+ SMALL-A
						(if (> remainder 1)
							main
							remainder)))
				base-26))
		(incf i))
	
	base-26
	;; return version letters
))
|#

(defun version-string-helper (year month day rel) ; defines version string itself {{{
	;; dates are expressed in a subset of ISO style
	;;   usually using "-" as delimiter and usually not "."
	(format nil "~A-~A-~A~a~a"  ; ex. 2022-10-17+a
		;; year, month, day - see VERSION struct definition
		(if (null year)  "????"  year)
		(if (null month) "?"  month)
		(if (null day)   "??"  day)
		
		;; include release number if specified;
		;; split into two forms to avoid nesting 'format function
		(if (null rel)  ""  "+")
		(if (null rel)
			""
			(release-integer-letter rel)))
) ; }}}

;;; VERSION data type {{{

(deftype date-field (max-value) ; NIL or positive INTEGER {{{
	(or  ; a DATE-FIELD contains either...
		nil  ; NIL, or...
		;; an integer from 1 to MAX-VALUE
		`(integer 1 ,max-value))
) ; }}}

(defstruct (version  ; year month day release {{{
	(:print-function
		;; 'V: version struct ; 'STRM: output stream
		(lambda (v strm depth)
			(declare (ignore depth))  ; ignore unused argument
			(format strm "#<VERSION ~a>"
				(version-string-helper
					(version-year v) (version-month v) (version-day v) (version-release v))
					)))
	)
	
	;; year - positive integer 1 or greater, or NIL
	;; this means no version can exist in the year 0, but when versions are generally expected to use a year after 1970 this should not cause any problems
	(year  (:type (date-field)))
	
	;; month - positive integer 1-12, or NIL
	(month (:type (date-field 12)))
	;; day - positive integer 1-31, or NIL
	(day   (:type (date-field 31)))
	
	;; these date fields should be based on the UTC (+0000) time zone,
	;; on the calendar defined in ISO 8601
	;; https://en.wikipedia.org/wiki/ISO_8601#Calendar_dates
	
	(release  (:type (date-field)))
	;; positive integer if present, or NIL if unspecified
	;; if unspecified, generally refers to a day's first release "a" / 1
) ; }}}

;;; }}}

;;; convert other things to VERSION {{{

(defun sequence-to-version (version-object) ; convert list/vector to VERSION {{{
	;; if passed some kind of list or vector like '(2022 10 17 1),
	;; assume it reads as '(year month day release)
	(make-version
		:release  ; put first for readability
			(if (< (length version-object) 4)
				nil
				(elt version-object 3))
		
		:year  (elt version-object 0)
		:month (elt version-object 1)
		:day   (elt version-object 2))
) ; }}}

;; LATER: actually implement this
(defun string-to-version (version-object) ; {{{
	;; try to parse the version string
	;; "-" is the preferred delimiter, but in practice you might see ".",
	;; in particular when trying to work with APIs designed for point versions.
	
	'()
) ; }}}

(defun time-to-version (version-object) ; convert universal time to VERSION {{{
	;; a version can /conceivably/ be stored as a UNIVERSAL-TIME,
	;; although this would be awkward for anything but comparing two versions.
	
	(multiple-value-bind
		(sec minute hour date month year) (decode-universal-time version-object 0)
			(make-version
				;; assume all seconds past midnight represent release number
				;; this allows for a maximum of about 43200 release numbers per day.
				:release  (+ (* 60 60 hour) (* 60 minute) sec)
				;; everything else is literal
				:year year  :month month  :day date))
) ; }}}

(defun parse-version (version-object) ; convert other formats to VERSION {{{
	(cond
		;; if passed a sequence, ex. '(2022 10 17 1), or its vector
		((or (listp version-object) (sequencep version-object))
			(sequence-to-version version-object))
		;; string - see 'version-string-helper
		;; ((stringp version-object)
		;; 	(string-to-version version-object))
		;; if passed a single integer, assume it's a UNIVERSAL-TIME
		((integerp version-object)
			(time-to-version version-object))
		(t nil))
	
	;; LATER: make sure release number is stored as integer in all cases
) ; }}}

;;; }}}

;;; compare and manipulate VERSION structs {{{

(defun version-to-time (version &key a) ; VERSION to universal time {{{
	(+  ; separately calculate date & release number, then add
		(encode-universal-time
			0 0 0  ; leave second, minute, and hour blank for now
			(version-day version)
			(version-month version)
			(version-year version)
			0)  ; versions are generally in UTC (+0000)
		
		;; if specified, use release "a"/1
		(if (null a)
			;; otherwise add on release number as number of seconds
			(version-release version)
			1))
) ; }}}


(defun next-release (version) ; bump release number of passed VERSION {{{
	(let ((next
		(copy-version version)))
		
		(incf (version-release next))  ; increment release
		next  ; return new VERSION
)) ; }}}

(defun release-a (version  &key when-empty) ; return same VERSION with release +a {{{
	(let ((initial
		(copy-version version)))
		
		(when (or
				(null when-empty)
				(null (version-release initial)))
			(setf (version-release initial) 1))  ; set release to 1
		
		initial  ; return new VERSION
)) ; }}}

;; version comparisons - version=, version< / > {{{
(defun version= (x y)
	(and
		(equal (version-to-time x :a t) (version-to-time y :a t))
		;; compare release numbers considering NIL the same as 1
		(equal
			(or (version-release x) 1)
			(or (version-release y) 1)
			))
)

(defun version< (x y)
	(<
		(version-to-time (release-a x :when-empty t))
		(version-to-time (release-a y :when-empty t)))
)

(defun version> (x y)
	(version< y x))
;; LATER: can you assign custom >/< to basic ones like with defsetf ?
;; }}}

(defun get-calendar-version (&key previous) ; get CalVer for current date {{{
	;; get current date with release number "a"/1
	(let ((current
		(release-a (time-to-version (get-universal-time)))
		))
	
	(if (null previous)
		;; if passed nothing, return current date with release "a"/1
		current
		;; if passed a clearly old date as "previous version"...
		(if (version< previous current)
			current  ; return current date
			(next-release previous)
			;; if date is not in the past, bump release number
			))
)) ; }}}

;;; }}}
