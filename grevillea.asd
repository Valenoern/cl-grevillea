(defsystem "grevillea"
	:author "Valenoern"
	:licence "AGPL"
		;; I'm not totally sure how necessary AGPL is on this one but there might be some chance it gets transpiled to the web with bopwiki or shardquest, so, yeah
	:description "library for v-refracta linux distribution, bopwiki, etc"
	
	:components (
		(:file "grevillea-common")  ; :grevillea-common - :common-lisp counterpart
		(:file "asdf")  ; :asdf/user [system package]
		(:file "grevillea")  ; :grevillea
		(:file "calver")  ; :vskal
		
		;; although there is an asdf extension to specify packages in asdf.lisp,
		;; it's best not to make use of it here so there are no complications in loading this system at the top of any systems that do use the extension
))
