(defpackage :grevillea
	;; it is not recommended to :use this library into other packages
	;; currently everything that uses it will use it by nickname
	(:use :common-lisp)
	;;(:local-nicknames
		;; :asdf
	;;)
	(:export
		;; macros
		#:letf
		
		;; debugging
		#:printl
		;; hash tables
		#:puthash #:debug-hash
		
		#:load-extension
))
(in-package :grevillea)


;;; macros {{{

(defmacro letf (bindings &rest forms) ; let* without parentheses, just like setf/setq {{{
	;; expand BINDINGS into BINDING-ARGS thus:
	(let ((binding-args
		(loop :for (a b) :on bindings :by #'cddr :while b
			:collect (list a b))))
	
		;; form returned by the macro
		`,(cons 'let* (cons binding-args forms))
		;; (let* ((arg1 value) (arg2 value))  forms)
	
	;; https://stackoverflow.com/questions/40028348/common-lisp-loop-through-pairs-of-a-list
	;; https://lisp-journey.gitlab.io/blog/common-lisp-macros-by-example-tutorial/
)) ; }}}

;;; }}}


(defun printl (&rest args) ; shorthand for (print (list)) {{{
"Shorthand for (print (list)).
ARGS - a list of arguments of any length, which will all be printed as a list."
	(print args)
) ; }}}


;;; hash tables - puthash , debug-hash {{{

(defun puthash (key value table) ; set a hash key as in emacs {{{
"set a hash element, as in emacs API

KEY - hash key to set
VALUE - value to set it to
TABLE - table to put hash key in"

	(setf (gethash key table) value)
) ; }}}

(defun debug-hash (table) "print contents of a hash table" ; {{{
	;; https://www.tutorialspoint.com/lisp/lisp_hash_table.htm
	(maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) table)
) ; }}}

;;; }}}



;;; asdf-related tools

(defun load-extension (&rest system-list) ; load optional systems if they exist {{{
"Load one or more asdf systems named in SYSTEM-LIST for use as extensions to an application, skipping any that were not found. System names should be in the same format as for asdf:load-system."

	;; allow passing a single system name, but turn it into a list
	(unless (listp system-list)
		(setq system-list (list system-list))
		)
	
	;; for each system in list that is not found, catch error and print warning
	(dolist (sys-name system-list)
		(handler-case (asdf:load-system sys-name)
			(error (c)
				c  ; unused required argument, leave alone
				(format t "Extension was not loaded: ~a~%" sys-name)
		))
	;; you can get information from an error with expressions like  (values 0 c)
	;; but that's not necessary here
)) ; }}}
