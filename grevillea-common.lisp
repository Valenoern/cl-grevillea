;; This package is specifically meant to be :USE'd and is specifically intended as v-refracta's counterpart to :common-lisp.
;; However, it is provided strictly as an unfinished alpha version and WITHOUT ANY GUARANTEE it will not collide with symbols in your packages. You may report issues at the online repository for help resolving them, but you are responsible for checking for these conflicts and bugs yourself.
(defpackage :grevillea-common
	(:use :common-lisp)
	(:export
		;; keyword for use in extended/specialised lambda lists
		#:&flag
))
(in-package :grevillea-common)

;; more things may be added here later, moved from :grevillea, etc
